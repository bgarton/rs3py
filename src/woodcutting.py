import pyautogui
from time import sleep
from lib import *

scriptType = "WOODCUTTING"
scriptDesc = "Currently powerchopping only.\n\nClick button 1 below then the tree,\nbutton 2 followed by very\ntop-left inventory GUI"
hotbarRegion = (900,1322,600,150)
inventoryRegion = (1828,1055,175,300)
itemImg = getImgDir() + "teaklogs.png"
itemImgDark = getImgDir() + "teaklogsdark1.png"

#tl 1800,1000
#br 2000,1320
  

def main():
  myGui(scriptType,scriptDesc)
  # Count our items
  iterations = sum(1 for _ in pyautogui.locateAllOnScreen(itemImg, confidence=0.85, region=inventoryRegion))
  x,y,darkItem = getHotbarOneKey(itemImg,itemImgDark,hotbarRegion)

  for i in range(iterations):
    if (x is None):
      print("Can't find one key! Have you placed the specified item on it?")
      exit()
    if (darkItem == False):
      pyautogui.moveTo(x,y)
      pyautogui.click(button='right')
      sleepv(rando(8,17))
      pyautogui.click(button='left')
    sleepv(rando(100,164))

  # Done, click Tree to continue at center of screen
  clickCenter()

for i in range(100):
  main()
  sleeptime = rando(25000, 53000)
  print("Goodnight... " + str(sleeptime))
  sleepv(sleeptime)
