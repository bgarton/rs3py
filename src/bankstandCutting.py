# Crafting
# Place uncut gems into new tab directly one after another, so it can see them in a row.
# In progress: Search for CANCEL button, if its gone then we are done.
from lib import *
import tkinter as tk
from tkinter.constants import DISABLED
import pyautogui
from math import floor

scriptType = "CUTTING"
scriptDesc = "Click the 1st button and the bank.\n\nClick 2nd button and the top\nleft corner of the inventory UI.\n\nThird button for the position of\nyour uncut gems."
depositBtnImg = getImgDir()+"depositInventory.png"
center = getCenter()
bankRegion = (center[0]-500, center[1]-500, center[0]+500, center[1]+500)
gemAmount = 140

##### GUI #####
invPos, bankInvPos, invRegion, nodePos = myGui(scriptType,scriptDesc)
inv1Pos = invPos[0]+rando(9,35), invPos[1]+rando(9,35)
print("Inv1pos",inv1Pos)
confidence = 0.80
sleepv(500)
##### END GUI #####    

def clickNode(nodePos):
  x = rando(nodePos[0]-6, nodePos[0]+6)
  y = rando(nodePos[1]-6, nodePos[1]+6)
  pyautogui.click(button="left",x=x,y=y)

def getDepositBtnPos():
  res = pyautogui.locateCenterOnScreen(depositBtnImg, confidence=confidence)
  if res:
    return res
  else:
    print("Failed to find deposit button! Exiting.")
    exit()

##### MAIN #####
def main():
  print("We have " + str(gemAmount) + " gems to cut...")
  iterations = floor(gemAmount / 28)
  for i in range(iterations):
    print("Iteration " + str(i) + "/" + str(iterations))
    clickNode(nodePos)
    sleepv(rando(1250,1550))
    pyautogui.click(getDepositBtnPos())
    sleepv(rando(1250,1550))
    pyautogui.click(bankInvPos)
    sleepv(rando(1250,1550))
    pyautogui.press('Esc')
    sleepv(rando(1250,1550))
    pyautogui.click(inv1Pos[0]+rando(9,35), inv1Pos[1]+rando(9,35)) # 9 - 35
    sleepv(rando(1250,1550))
    pyautogui.press('Space')
    print("Sleeping...")
    sleepv(rando(20000,25000))
  
main()