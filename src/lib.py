import pyautogui
import random
from time import sleep
from math import floor
import win32api, win32gui
import requests, json #http webhooks
import tkinter as tk

def getImgDir():
  return "../media/"

def sleepv(input):
  sleept = floor(input/1000)
  sleep(sleept)

# Random number for delays etc
def rando(min, max):
  # Weighting
  numbers = ['A'] + ['B'] + ['C'] * 2
  res = random.choice(numbers)
  if res == 'C':
    # 20% weighting
    min = floor(min * 1.0)
    max = floor(max * 1.0)
  rRange = random.randrange(min, max)
  return rRange

# Start looking for image
# input: func to call when found on the returned x,y

def getCenter():
  vx, vy = pyautogui.size()
  return rando((vx/2)-4, (vx/2)+4), rando((vy/2)-4, (vy/2)+4)

def clickCenter():
  vx, vy = getCenter()
  pyautogui.click(vx, vy)
  return True

# Find and click single image by name
def imgClick(img, type, confidence, region=None, xmin=0, xmax=0, ymin=0, ymax=0):
  # confidence 0.80+ for minimum accurate logs, 0.70 for inaccuracy
  if (region is not None):
    pos = pyautogui.locateOnScreen("media/" + img, confidence=confidence, region=region)
  else:
    pos = pyautogui.locateOnScreen("media/" + img, confidence=confidence)
  if pos[0] != None:
    x = pos[0]
    y = pos[1]
    if (xmin is not None):
      # bug if xmin = xmax
      if (xmin == xmax):
        xmax = xmax+1
      x = rando(x+xmin, x+xmax)

      if (ymin is not None):
        # bug if ymin = ymax
        if (ymin == ymax):
          ymax = ymax+1
        y = rando(y+ymin, y+ymax)

    pyautogui.moveTo(x, y)
    pyautogui.click(button=type)
    return True
  else:
    return False

def getHotbarOneKey(itemImg='', itemImgDark='', hotbarRegion='', confidence=0.7):
  if (itemImg == '' or itemImgDark == '' or hotbarRegion == ''):
    print("ERROR getHotbarOneKey not all params provided!")
    return
  pos = pyautogui.locateOnScreen(itemImgDark, confidence=confidence, region=hotbarRegion)
  darkItem = True
  if (pos is None):
    pos = pyautogui.locateOnScreen(itemImg, confidence=confidence, region=hotbarRegion)
    darkItem = False
  if (pos is None):
    print("Still can't find item image " + itemImg)
    return
  x = (pos.left + (pos.width/2))
  y = (pos.top + (pos.height-25))
  return x,y,darkItem

def getHotbarTwoKey(x,y):
  x = floor(x*1.033)
  pyautogui.moveTo(x, y)
  return x,y

def getHotbarThreeKey(x,y):
  x = floor(x*1.067)
  return x,y

def webHook(desc,title="RS3Py bot says..."):
  url = "https://discord.com/api/webhooks/837774712763056199/GDvinQosURX8JGXItuoWbuyeGHp7oGac7QgeFFDzIbc05kqhYXX53IC_j-GCsYcLc2eo"
  data = {}
  #for all params, see https://discordapp.com/developers/docs/resources/webhook#execute-webhook
  #data["content"] = "message content"
  data["username"] = "RS3Py"

  #leave this out if you dont want an embed
  data["embeds"] = []
  embed = {}
  #for all params, see https://discordapp.com/developers/docs/resources/channel#embed-object
  embed["description"] = desc
  embed["title"] = title
  data["embeds"].append(embed)

  result = requests.post(url, data=json.dumps(data), headers={"Content-Type": "application/json"})

  try:
      result.raise_for_status()
  except requests.exceptions.HTTPError as err:
      print(err)
  #else:
      #print("Payload delivered successfully, code {}.".format(result.status_code))
      #print("Msg:",desc)
def myGui(scriptType,scriptDesc):
  invPos = 1830,1057
  bankInvPos = 963,960
  invRegion = 1830,1058,175,300

  def closeGui():
    window.destroy()

  def enable_mouseposition():
    window.after(10, get_mouseposition)
  def get_mouseposition():
    global nodePos
    state_left = win32api.GetKeyState(0x01)
    if state_left == -127 or state_left == -128:
      #xclick, yclick = win32api.GetCursorPos()
      #nodePos = xclick, yclick
      x = pyautogui.position().x
      y = pyautogui.position().y
      print("1Func:",x,y)
      nodePos = x,y
      return pyautogui.position()
    else:
      window.after(10, get_mouseposition)

  def enable_mouseposition_inventoryNW():
    window.after(10, get_mouseposition_inventoryNW)
  def get_mouseposition_inventoryNW():
    global invPos
    global invRegion
    state_left = win32api.GetKeyState(0x01)
    if state_left == -127 or state_left == -128:
      #xclick, yclick = win32api.GetCursorPos()
      #invPos = xclick, yclick
      x = pyautogui.position().x
      y = pyautogui.position().y
      print("2Func:",x,y)
      invPos = x,y
      invRegion = invPos[0],invPos[1],175,300
      return pyautogui.position()
    else:
      window.after(10, enable_mouseposition_inventoryNW)

  def enable_mouseposition_bankinv1():
    window.after(10, get_mouseposition_bankinv1)
  def get_mouseposition_bankinv1():
    global bankInvPos
    state_left = win32api.GetKeyState(0x01)
    if state_left == -127 or state_left == -128:
      #xclick, yclick = win32api.GetCursorPos()
      #invPos = xclick, yclick
      x = pyautogui.position().x
      y = pyautogui.position().y
      print("3Func:",x,y)
      bankInvPos = x,y
      return pyautogui.position()
    else:
      window.after(10, enable_mouseposition_bankinv1)

  window = tk.Tk()
  window.wm_attributes("-topmost", 1)
  geometry = "310x310"
  if (scriptType == "CUTTING"):
    geometry = "310x310"
  window.geometry(geometry)
  window.title("RS3Py")

  lTitle = tk.Label(window, text = "RS3Py by RIDAI")
  lTitle.config(font =("Verdana", 14))
  lTitle.grid(sticky=tk.N)
  lTitle2 = tk.Label(window, text = scriptType)
  lTitle2.config(font =("Verdana", 12))
  lTitle2.grid(sticky=tk.N)
  txtInfo = tk.Text(window, height = 7, width = 37)
  txtInfo.grid(sticky=tk.NW, padx=5)
  txtInfo.insert(tk.INSERT,scriptDesc)
  if (scriptType == "CUTTING"):
    nodeTxt = "Bank Node"
  elif (scriptType == "MINING"):
    dropdownMiningStr = tk.StringVar(window)
    dropdownMiningStr.set("MINING-GEMS") # default value
    dropdownMining = tk.OptionMenu(window, dropdownMiningStr, "MINING-GEMS", "MINING-ORES")
    dropdownMining.grid(pady=4,column=0)
    nodeTxt = "Mining Node"
  else:
    nodeTxt = "Node"
  btnNodePos = tk.Button(window, text="1. Click "+nodeTxt+" pos", command=enable_mouseposition, width=25)
  btnNodePos.grid(pady=4,column=0)
  btnInvPos = tk.Button(window, text="2. Click NW Inventory UI pos", command=enable_mouseposition_inventoryNW, width=25)
  btnInvPos.grid(pady=4,column=0)

  if (scriptType == "CUTTING"):
    btnBankInvPos = tk.Button(window, text="3. Click uncut gem in bank", command=enable_mouseposition_bankinv1, width=25)
    btnBankInvPos.grid(pady=4,column=0)

  btnClose = tk.Button(window, text="Start", width=15, command=closeGui)
  btnClose.grid(pady=4,column=0)

  window.mainloop()
  print("GUI Closed\n----------")
  print("\n\nNode Position: " + str(nodePos) + "\nInventory NW Position: " + str(invPos) + "\nInventory Region: " + str(invRegion))
  if (scriptType == "CUTTING"):
    print("Bank Gems Position: " + str(bankInvPos))
  return invPos, bankInvPos, invRegion, nodePos, dropdownMiningStr.get()