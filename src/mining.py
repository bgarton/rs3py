# GUI
from PIL.ImageOps import grayscale
from lib import *
import win32api, win32gui
import pyautogui
import os

confidence = 0.6
scriptType = "MINING"
scriptDesc = "Hi " + os.getlogin() + ",\n\nClick the button and then the\ncenter of your node.\n\nSame for inventory, click the top\nleft of the inv UI."

##### GUI #####
invPos, bankInvPos, invRegion, nodePos, scriptSubType = myGui(scriptType,scriptDesc)
inv1Pos = invPos[0]+rando(9,35), invPos[1]+rando(9,35)
if scriptSubType == "MINING-GEMS":
  bagImg = getImgDir() + "gembag.png"
else:
  bagImg = getImgDir() + "orebox.png"
##### END GUI #####    

def getRs3Window():
  hwndMain = win32gui.FindWindow(None, "RuneScape")
  hwndEdit = win32gui.FindWindowEx( hwndMain, 0, "Edit", 0 )
  win32api.PostMessage( hwndEdit,win32con.WM_CHAR, ord('l'), 0)

  x=300
  y=300
  lParam = y <<15 | x
  #pycwnd = win32ui.CreateWindowFromHandle(hwnd)
  #whndl = win32gui.FindWindowEx(0, 0, None, 'RuneScape') 
  win32api.SendMessage(win32con.WM_LBUTTONDOWN, win32con.MK_LBUTTON, lParam);
  win32api.SendMessage(win32con.WM_LBUTTONUP, 0, lParam);

def bagCheck(scriptSubType):
  global confidence
  # Do we have a bag in our inv?
  if scriptSubType == "MINING-GEMS":
    res = pyautogui.locateCenterOnScreen(bagImg, confidence=confidence)
    print(res)
    if (res):
      print("Gem bag is at " + str(res.x) + ", " + str(res.y))
      return res
    else:
      print("Cannot find gem bag!")
      return 0
  else:
    print("Can't find bag, using first spot of inventory instead")
    return inv1Pos[0]+rando(10,35), inv1Pos[1]+rando(10,35)

def fillGemBag(gemBagPos):
  x = rando(gemBagPos[0]-3, gemBagPos[0]+3)
  y = rando(gemBagPos[1]-3, gemBagPos[1]+3)
  pyautogui.moveTo(x=x,y=y) # was right click
  sleepv(rando(165,225))
  pyautogui.click(button="left") # was right click
  #sleepv(rando(75,135))
  #pyautogui.moveRel(0, rando(19,32)) # 19-32
  #sleepv(rando(65,125))
  #pyautogui.click(button="left")

def clickNode(nodePos):
  x = rando(nodePos[0]-6, nodePos[0]+6)
  y = rando(nodePos[1]-6, nodePos[1]+6)
  pyautogui.click(button="left",x=x,y=y)

sleepv(500)

##### MAIN #####
print("scriptSubType",scriptSubType)
iRange = 5 # 5 bag-fulls
for i in range(iRange):
  webHook("Iteration " + str(i) + "/" + str(iRange))
  gemBagPos = bagCheck(scriptSubType)

  if (gemBagPos == 0):
    exit()

  #print("TEST")
  #getRs3Window()

  for i in range(rando(12,16)):
    clickNode(nodePos)
    if rando(1,10) > 3:
      sleepv(rando(2000,5000))
    else:
      sleepv(rando(6000,16000))
  
  fillGemBag(gemBagPos)
  sleepv(rando(100,200))
